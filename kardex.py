# kardex: kardex.py
# Macro para la generación del kardex general de los empleados.
#
# Copyright (C) 2016 William Ernesto Cárdenas Gómez
#                                                 <williamcardenas@cpapaseo.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>

import uno
from com.sun.star.beans import PropertyValue
from com.sun.star.sheet.DataImportMode import SQL
from com.sun.star.table.CellContentType import EMPTY

###################### Sección de clases auxiliares ###########################
class Formulario:
    """ Obtiene y establece informacion.
    Esta clase puede ser utilizada para acceder y establecer informacion
    almacenada en los formularios."""
	
    def __init__(self):
        """ Inicializacion de la clase """
        self._initial_date = '01/01/2000'
        self._final_date = '01/01/2000'
	
    def set_initial_date(self, date):
        """ Establece la fecha inicial """
        self._initial_date = date
		
    def set_final_date(self, date):
        """ Establece la fecha final. """
        self._final_date = date
	
    def get_initial_date(self):
        """ Obtener la fecha final """
        return self._initial_date
	
    def get_final_date(self):
        """ Obtiene la fecha final. """
        return self._final_date
	
class Format:
    """ Crea el formato.	
    Crea el formato base donde se debe trabajar """
    _title = ['A1', '']
    _subtitle = ['A3', '']
    _dates = ['A4', '']
    _table_headers = [
        ['A6', ''],
        ['B6', ''],
        ['C6', ''],
        ['D6', ''],
        ['E6', ''],
        ['F6', ''],
        ['G6', ''],
        ['H6', ''],
        ['I6', ''],
        ['J6', ''],
        ['K6', ''],
        ['L6', ''],
        ['M6', '']
        ]
    
    def __init__(self, document, sheet_name):
        """ Inicializador de la clase """
        self.document = document
        self._create_sheet(sheet_name)
        self.set_title('Caja Popular Apaseo el Alto SC de AP de RL de CV')
        self.set_subtitle('KARDEX DE ASISTENCIA')
        self.set_dates('')
        self.set_table_headers([
            'UID',
            'EMPLEADO',
            'FECHA',
            'ENT. 1',
            'SAL. 1',
            'H. TR.',
            'ENT. 2',
            'SAL. 2',
            'H. TR.',
            'ENT. 3',
            'SAL. 3',
            'H. TR.',
            'TOTAL'])
        self.clean_table()

    def _create_sheet(self, sheet_name):
        """ Crea la nueva hoja.

        Si no existe la crea en caso de existir solo toma la referencia. """
        if not self.document.getSheets().hasByName(sheet_name):
            self.document.getSheets().insertNewByName(sheet_name, 1)
        self.set_report(sheet_name)

    def clean_table(self):
        """ Limpia los datos de la tabla. """
        table = self._report.getCellRangeByName('A7:M1048576')
        table.clearContents(23)

    def set_report(self, report_sheet_name):
        """ Selecciona la hoja donde se imprimira el reporte """
        self._report = self.document.getSheets().getByName(report_sheet_name)

    def print_content(self, cell):
        """ Imprime el contenido en una celda .
        Solo imprime strings """
        self._report.getCellRangeByName(cell[0]).setString(cell[1])

    def set_title(self, title):
        """ Imprime y establece el titulo """
        self._title[1] = title
        self.print_content(self._title)

    def set_subtitle(self, subtitle):
        """ Imprime y establece el subtitulo """
        self._subtitle[1] = subtitle
        self.print_content(self._subtitle)

    def set_dates(self, dates):
        """ Imprime y establece el rango de fechas """
        self._dates[1] = dates
        self.print_content(self._dates)

    def set_table_headers(self, table_headers):
        """ Imrpime y establece los encabezados """
        i = 0
        for header in table_headers:
            self._table_headers[i][1] = header
            i = i + 1
        for cell in self._table_headers:
            self.print_content(cell)

class Registro:
    """ Representa un renglón o registro en la hoja """
    _registros = []

    def __init__(self, renglon):
        """ Inicializador de clase """
        self._renglon = str(renglon)
        self.set_uid(0)
        self.set_nombre('')
        self.set_fecha('')
        self._registros = []

    # Funciones operativas
    def print_row(self, sheet):
        """Imprime un registro.

            La información debe estar previamente cargada, de lo cotrario
            solo se imprimirá en blanco."""
        r = self.get_registros()
        total1 = 0
        total2 = 0
        total = 0
        n_row = self._renglon
        letra = ["D", "E", "F", "G", "H",
                 "I", "J", "K", "L", "M", "N",
                 "O", "P", "Q", "R", "S", "T",
                 "U", "V", "W", "X", "Y", "Z"]
        li = 0
        sheet.getCellRangeByName("A" + self._renglon).setValue(self.get_uid())
        sheet.getCellRangeByName("B" + self._renglon).setString(
            self.get_nombre())
        sheet.getCellRangeByName("C" + self._renglon).setString(
            self.get_fecha())
        d = XSCRIPTCONTEXT.getDocument().getSheets().getByName("Formularios")
        index = 0
        for item in r:
            if index % 2 == 0:
                total1 = item
                sheet.getCellRangeByName(letra[li] + self._renglon).setString(
                    min_to_hr(item))
                li = li + 1
            else:
                total2 = item - total1
                total = total + total2
                sheet.getCellRangeByName(letra[li] + self._renglon).setString(
                    min_to_hr(item))
                sheet.getCellRangeByName(
                    letra[li+1] + self._renglon).setString(
                        min_to_hr(total2))
                li = li + 2
            index = index + 1
        sheet.getCellRangeByName("M" + self._renglon).setString(
            min_to_hr(total))

    # Funciones Set y Get
     # Funciones Set
    def set_uid(self, uid):
        """Establece el UID"""
        self._uid = uid

    def set_nombre(self, nombre):
        """Establece el Nombre"""
        self._nombre = nombre

    def set_fecha(self, fecha):
        """Establece la fecha"""
        self._fecha = fecha[0:8]

    def set_registros(self, registros, tolerancia):
        """Establece los registros de horas de entrada/salida.

        De las entradas se leen la que tuvo el empleado ese día y se selecciona
        las que considera adecuadas."""
        regs = []
        for hora in registros:
            regs.append(hr_to_min(hora))
        regs.sort()
        matriz = self.cargar_matriz(regs, tolerancia)
        i_regs = 1
        for i in range(0, len(matriz)):
            if i % 2 == 0:
                self._registros.append(matriz[i][0])
            else:
                self._registros.append(matriz[i][len(matriz[i]) - 1])

     # Funciones get
    def get_uid(self):
        """Obtiene el uid"""
        return self._uid

    def get_nombre(self):
        """Obtiene el nombre del empleado"""
        return self._nombre

    def get_fecha(self):
        """Obtiene la fecha del registro"""
        return self._fecha

    def get_registros(self):
        """Obtiene los registros de ese día (Checadas)"""
        return self._registros

    ### Funciones Estaticas
    @staticmethod
    def cargar_matriz(lista, n):
        """Matriz con valores agrupados por proximidad.

           Si por ejemplo se da un valor n = 20 Va a crear registros en donde
           cada valor no tendrá mas de 20 minutos entre uno y otro, al
           detectarse una mayor separación se inicia un nuevo renglón."""
        aux = lista[0]
        matriz = []
        row = [lista[0]]
        for item in lista[1:]:
            if item - aux >= n:
                matriz.append(row)
                row = []
            row.append(item)
            aux = item
        matriz.append(row)
        return matriz

###################### Funciones ##############################################
## Funciones Lambda
hr_to_min = lambda hora: (int(hora[9:11])*60)+int(hora[12:14])
min_to_hr = lambda minutos: '{:0>2d}:{:0>2d}'.format(minutos//60, minutos%60)

## Funciones
def makeSheet(calling):
    """ Función principal (inicial).

    Inicia la ejecución de todo el script. Inicialmente crea la nueva hoja,
    posteriormente, lelna dicha hoja con los datos obtenidos."""
    sheet_name = 'reporte'
    form = Formulario()
    documento = XSCRIPTCONTEXT.getDocument()
    main_sheet = documento.getSheets().getByName('Formularios')
    form.set_initial_date(main_sheet.getCellRangeByName('B1').getString())
    form.set_final_date(main_sheet.getCellRangeByName('B2').getString())
    mformat = Format(documento, sheet_name)
    mformat.set_dates('DEL: ' + form.get_initial_date() +
                      ' AL: ' + form.get_final_date())
    hoja = documento.getSheets().getByName(sheet_name)
    fill_table(documento.getSheets().getByName(sheet_name),
               form.get_initial_date(),
               form.get_final_date())

def create_temp(initial_date, final_date):
    """ Crea una hoja temporal

    El kardex sin tratar se almacena en esa hoja"""
    consulta = "SELECT u.USERID, u.NAME, ch.CHECKTIME " + \
               "FROM USERINFO u INNER JOIN CHECKINOUT ch " + \
               "ON u.USERID = ch.USERID " + \
               "WHERE ch.CHECKTIME >= {d '" + initial_date + "'} " + \
               "AND ch.CHECKTIME <= {d '" + final_date + "'} " + \
               "ORDER BY u.USERID ASC, ch.CHECKTIME ASC"
    PropertyValue1 = PropertyValue()
    PropertyValue1.Name = "DatabaseName"
    PropertyValue1.Value = "Checadores"
    PropertyValue2 = PropertyValue()
    PropertyValue2.Name = "SourceType"
    PropertyValue2.Value = SQL
    PropertyValue3 = PropertyValue()
    PropertyValue3.Name = "SourceObject"
    PropertyValue3.Value = consulta
    Conn = (PropertyValue1, PropertyValue2, PropertyValue3)
    
    XSCRIPTCONTEXT.getDocument().getSheets().insertNewByName("temporal", 2)
    tmp_sheet = XSCRIPTCONTEXT.getDocument().getSheets().getByName("temporal")
    cell = tmp_sheet.getCellRangeByName("A1")
    cell.doImport(Conn)

def elimina_temp():
    """ Elimina la hoja temporal """
    XSCRIPTCONTEXT.getDocument().getSheets().removeByName("temporal")

def fill_table(sheet, initial_date, final_date):
    """ Llena el registro de entradas y salidas. """
    create_temp(initial_date, final_date)
    create_list()
    elimina_temp()

def create_list():
    """ Crea lista de entradas y salidas.

    Lee los datos originales desde la hoja 'temporal' y los organiza en la hoja
    'reporte'. Esta función se vale de la clase Registro para realizar la
    impresión en la hoja reporte. """
    doc = XSCRIPTCONTEXT.getDocument()
    hoja = doc.getSheets().getByName("temporal")
    salida = doc.getSheets().getByName("reporte")
    formulario = doc.getSheets().getByName("Formularios")
    tolerancia = formulario.getCellRangeByName("B3").getValue()
    row = 2
    record_row = 7
    date_aux = "00/00/00"
    name_aux = ""
    reg_temp = []
    registro_anterior = Registro(record_row)
    while hoja.getCellRangeByName("A" + str(row)).getType() != EMPTY:
        date = hoja.getCellRangeByName("C" + str(row)).getString()
        name = hoja.getCellRangeByName("B" + str(row)).getString()
        if date_aux != date[0:8] or name_aux != name:
            if len(reg_temp) > 0 :
                registro_anterior.set_registros(reg_temp, tolerancia)
                registro_anterior.print_row(salida)
                registro_anterior = 0
                reg_temp = []
            registro = Registro(record_row)
            record_row = record_row + 1
            registro.set_uid(
                hoja.getCellRangeByName("A" + str(row)).getValue())
            registro.set_nombre(hoja.getCellRangeByName(
                "B" + str(row)).getString())
            registro.set_fecha(date)
            registro_anterior = registro
            date_aux = date[0:8]
            name_aux = name
        reg_temp.append(date)
        row = row + 1
    if len(reg_temp) > 0 :
        registro_anterior.set_registros(reg_temp, tolerancia)
        registro_anterior.print_row(salida)
        registro_anterior = 0
        reg_temp = []
